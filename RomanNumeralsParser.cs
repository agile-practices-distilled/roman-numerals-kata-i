﻿using System;

namespace RomanNumerals_I
{
    public class RomanNumeralsParser
    {
        public static string Parse(int number)
        {
            if (number >= 50)
                return "L" + Parse(number - 50);
            if (number >= 10)
                return "X" + Parse(number - 10);
            if (number == 9)
                return "IX";
            if (number >= 5)
                return "V" + Parse(number - 5);
            if (number == 4)
                return "IV";
            if (number >= 1)
                return "I" + Parse(number - 1);
            if (number == 0)
                return "";

            return "";
        }
    }
}