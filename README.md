# Roman numerals kata I

Write a function to convert Arabic numbers to Roman numerals as best as you can, following the TDD practices we have been using.43
Given a positive integer number (for example, 42), determine its Roman numeral represen- tation as a string (for example, “XLII”). You cannot write numerals like IM for 999.